# ansible-netdc-switch-base

Ansible role to apply a base config on a bare installation of Cumulus Linux on
a switch. This role does not apply a network configuration.

## Variables

By default the [HTTP REST API](https://docs.cumulusnetworks.com/cumulus-linux-37/System-Configuration/HTTP-API/)
is disabled. Enable it with this variable:

```yaml
restapi_enabled: true
```

You can configure the hsflow daemon for sFlow data. Enable and configure it
with these variables:

```yaml
hsflow_enabled: true
sflow_collector: <ip_address>
```

### Prometheus

You can configure the address and VRF on which node_exporter should be
available for scraping:

```yaml
prometheus_node_exporter_address: 1.2.3.4
prometheus_node_exporter_vrf: mgmt
```

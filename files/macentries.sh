#!/bin/bash

MAC_TABLE=$(/usr/bin/net show bridge macs)
echo "# HELP cumulus_mac_entries Number of addresses in MAC table."
echo "# TYPE cumulus_mac_entries gauge"
VLANS=$(echo "$MAC_TABLE" | tail -n +4 | awk '{print $1}' | sort | uniq)
for VLAN in $VLANS
do
  VLAN_ENTRIES=$(echo "$MAC_TABLE" | grep -c '^'"$VLAN"' ')
  echo "cumulus_mac_entries{vlan=\"$VLAN\"} $VLAN_ENTRIES"
done

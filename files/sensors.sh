#!/bin/bash

SENSORS_DATA=$(/usr/bin/net show system sensors json)
echo "# HELP cumulus_sensor_state Sensor state on Cumulus devices."
echo "# TYPE cumulus_sensor_state gauge"
for i in $(echo "$SENSORS_DATA" | jq -c '.[] | {name,state}')
do
  if [ "$(echo "$i" | jq -r '.state')" == "OK" ]
  then
    SENSOR_STATE=0
  elif [ "$(echo "$i" | jq -r '.state')" == "BAD" ]
  then
    SENSOR_STATE=1
  else
    SENSOR_STATE=2
  fi
  echo "cumulus_sensor_state{sensor=$(echo "$i" | jq .name)} $SENSOR_STATE"
done
